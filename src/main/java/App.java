import persistance.JDBCImplementation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class App {

    public static void main(String[] args) {
        JDBCImplementation jdbc = new JDBCImplementation();
        Connection connection = jdbc.getConnection();

        try {
            connection.createStatement().execute(
                    "UPDATE Table1 SET metai = metai + 50 WHERE vardas LIKE '%o%' "
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(
                    "SELECT * FROM Table1");
            while (resultSet.next()) {
                System.out.print(resultSet.getInt("id") + " ");
                System.out.print(resultSet.getString("vardas") + " ");
                System.out.print(resultSet.getString("pavarde") + " ");
                System.out.print(resultSet.getInt("metai") + " ");
                System.out.println();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
